@mitchallen/collect-email
==
PUT DESCRIPTION HERE
--

<p align="left">
  <a href="https://travis-ci.org/mitchallen/collect-email">
    <img src="https://img.shields.io/travis/mitchallen/collect-email.svg?style=flat-square" alt="Continuous Integration">
  </a>
  <a href="https://codecov.io/gh/mitchallen/collect-email">
    <img src="https://codecov.io/gh/mitchallen/collect-email/branch/master/graph/badge.svg" alt="Coverage Status">
  </a>
  <a href="https://npmjs.org/package/@mitchallen/collect-email">
    <img src="http://img.shields.io/npm/dt/@mitchallen/collect-email.svg?style=flat-square" alt="Downloads">
  </a>
  <a href="https://npmjs.org/package/@mitchallen/collect-email">
    <img src="http://img.shields.io/npm/v/@mitchallen/collect-email.svg?style=flat-square" alt="Version">
  </a>
  <a href="https://npmjs.com/package/@mitchallen/collect-email">
    <img src="https://img.shields.io/github/license/mitchallen/collect-email.svg" alt="License"></a>
  </a>
</p>

## Installation

    $ npm init
    $ npm install @mitchallen/collect-email --save
  
* * *

## Usage

## Testing

To test, go to the root folder and type (sans __$__):

    $ npm test
   
* * *
 
## Repo(s)

* [bitbucket.org/mitchallen/collect-email.git](https://bitbucket.org/mitchallen/collect-email.git)
* [github.com/mitchallen/collect-email.git](https://github.com/mitchallen/collect-email.git)

* * *

## Contributing

In lieu of a formal style guide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality. Lint and test your code.

* * *

## Version History

#### Version 0.1.0 

* initial release

* * *
